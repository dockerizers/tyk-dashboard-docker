# Tyk Dashboard Docker Image with Telegraf and Git setup

The Repo has two branches. `master` and `release`. Once a release branch is updated, the docker image should be built and pushed to the gitlab docker registry.

Image: registry.gitlab.com/dockerizers/tyk-dashboard-docker

```yaml
environment:
    - TYK_DB_CONF=/opt/tyk-dashboard/tyk_analytics.conf
    - TYK_DB_TYKAPI_SECRET=ewweew
    - TYK_DB_NODESECRET=werewr
    - TYK_DB_ADMINSECRET=erwewrew
    - GD_Key=$GD_Key
    - GD_Secret=$GD_Secret
    - LE_CONFIG_HOME=/etc/volume/acme.sh
    - DASHBOARD_DOMAIN=your.dash-domain.com

volume:
    - ./config/tyk_analytics.conf:/opt/tyk-dashboard/tyk_analytics.conf
    - ./secrets/dashboard/ssl:/etc/certs
    - ./config/telegraf/dashboard.conf:/etc/telegraf/telegraf.conf:ro
    - ./volumes/dashboard/acme.sh:/etc/volume/acme.sh

entrypoint:
    - bash
    - -c
    - |
      service telegraf start 
      /root/.acme.sh/acme.sh --issue --dns dns_gd -d $${DASHBOARD_DOMAIN} -d www.$${DASHBOARD_DOMAIN}
      /opt/tyk-dashboard/tyk-analytics --conf=$${TYK_DB_CONF}
```

# Let's Encrypt
Set up godaddy Keys from [here](https://github.com/Neilpang/acme.sh/wiki/dnsapi#4-use-godaddycom-domain-api-to-automatically-issue-cert)

Once done on your local host export the GoDaddy api credentials as ENV variables so that docker compose can pick them up when starting

```bash
export GD_Key="8df7a0987df98adsf"
export GD_Secret="a70df98dsf7"
```

You can add a file `~/.bash_env` with the above content and then append `~/.bash_profile` with the following

```bash
...
source ~/.bash_env
```

# Image Versioning
When pushing a new image, ensure that teh VERSION variable in `.gitlab-ci` matches the docker image version. 