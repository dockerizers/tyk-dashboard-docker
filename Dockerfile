FROM tykio/tyk-dashboard:v1.9.3
LABEL maintainer="Kariuki Gathitu <kgathi2@gmail.com>"
LABEL version="1.0"

ENV DISTRO_NAME=buster

# Before adding telegraf Influx repository, run this so that apt will be able to read the repository.
RUN apt-get update && apt-get install -y apt-transport-https wget 
# Add the telegraf InfluxData key
RUN wget -qO- https://repos.influxdata.com/influxdb.key | apt-key add -
RUN echo "deb https://repos.influxdata.com/debian ${DISTRO_NAME} stable" | tee /etc/apt/sources.list.d/influxdb.list

# procps for ps in k8 probes
ENV CORE_PACKAGES="telegraf procps"

RUN apt-get update && apt-get install -y $CORE_PACKAGES && apt-get clean